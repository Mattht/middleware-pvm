/* 
 * File:   BankProcess.cpp
 * Author: mattht
 *
 */

#include <cstdlib>
#include <iostream>
#include <string.h>
#include <fstream>
#include <stdio.h>
#include "pvminc.h"
#include "networkMessages.h"
#include "Bank.h"
#include "BankAccount.h"

using namespace std;

int main(int argc, char** argv){
    
    Bank *bank;
    
    int mytid, ctid, nametid, children[10];
    int info;
    
    mytid = pvm_mytid();
    ctid = atoi(argv[1]);
    
    bank = new Bank( atoi(argv[2]), argv[3] );
    
    BankAccount *bankAccList[3];
    
    switch( bank->GetId() )
    {
        case 101:
            bankAccList[0] = new BankAccount( 101, "Current", 1000 );
            bankAccList[1] = new BankAccount( 102, "Savings", 3400 );
            bankAccList[2] = new BankAccount( 103, "Personal", 50 );
            break;
        case 102:
            bankAccList[0] = new BankAccount( 201, "Current", 2000 );
            bankAccList[1] = new BankAccount( 202, "Savings", 1500 );
            bankAccList[2] = new BankAccount( 203, "Personal", 4050 );
            break;
        case 103:
            bankAccList[0] = new BankAccount( 301, "Current", 3000 );
            bankAccList[1] = new BankAccount( 302, "Savings", 1010 );
            bankAccList[2] = new BankAccount( 303, "Personal", 2040 );
            break;   
    }
    
    ofstream myFile;
    
    char username[20];
    char password[20];
    int nbBankAccount = -1;
    
    bool userConnected = false;
    
    char testUsername[20];
    char testPassword[20];
    
    strcpy( testUsername, "Mattht" );
    strcpy( testPassword, "azerty" );
    
    while( nbBankAccount == -1 )
    {
        
        pvm_recv( -1, LOGIN );
        pvm_upkstr( username );
        pvm_upkstr( password );
        
        if( ( strcmp( username, testUsername ) == 0 && strcmp( password, testPassword  ) == 0 ) 
            || ( strcmp( username, (char*)"admin" ) == 0 && strcmp( password, (char*)"admin"  ) == 0 ) )
        {
            userConnected = true;
            
            nbBankAccount = 3;
            
            char nodeInfo[10];
                
            if( bank->GetId() == 101 )				// Sets node depending on bank Id
            {
                strcpy( nodeInfo, "node04" );
            }
            else if ( bank->GetId() == 102 )
            {
                strcpy( nodeInfo, "node05" );
            }
            else if ( bank->GetId() == 103 )
            {
                strcpy( nodeInfo, "node06" );
            }
            else
                strcpy( nodeInfo, "node04" );
            
            
            for( int i = 0; i < nbBankAccount; i++ )
            {
                char **toSend;

                toSend = new char*[5];
                for(int j = 0; j < 5; j++)
                    toSend[j] = new char[10];

                sprintf( toSend[0], "%d", ctid );                               // Client tid
                sprintf( toSend[1], "%d", bankAccList[i]->GetId() );            // Account Id
                strcpy( toSend[2], bankAccList[i]->GetName() );                 // Account Name
                sprintf( toSend[3], "%d", bankAccList[i]->GetBalance() );       // Account Balance
                toSend[4] = (char*)0;

                info = pvm_spawn(
                    (char*)"/homes/12007734/pvm/bankAccount",
                    toSend,
                    PvmTaskHost,
                    nodeInfo,
                    1,
                    &children[i]                                                // Passing the array does not work
                    );
            }
        }
        
        pvm_initsend( PvmDataDefault );
        pvm_pkint( &nbBankAccount, 1, 1 );
        /*if( nbBankAccount != -1 )
        {
            pvm_pkint( children, nbBankAccount, 1 );            // Sends an array;
        }*/
        /*
        myFile.open("./bankInfo", std::ios_base::app);
        myFile << "TID: " << children[i] << "\n";
        myFile << "ID: " << cId[i] << "\n";
        myFile << "Name: " << cName[i] << "\n";
        myFile << " ------ \n";
        myFile.close();
        */
        
        if( nbBankAccount != -1 )                                               // Sending accounts info to client
        {
            for( int i = 0; i < nbBankAccount; i++ ) {
                
                pvm_pkint( &children[i], 1, 1 );
                pvm_pkint( bankAccList[i]->GetIdForMsg(), 1, 1 ); // ACCOUNT ID
                pvm_pkstr( bankAccList[i]->GetName() ); // ACCOUNT NAME
            }
        }
        
        pvm_send( ctid, LOGINR );
    }
    
    while( userConnected ) {
        
        int chosenOp;
        int chosenAc;
        int chosenAm;
        
        int newBalance = -1;
        int success = 0;
        
        pvm_recv( -1, ACCOUNTOP );  /* receive data */
        pvm_upkint( &chosenOp, 1, 1 );
        pvm_upkint( &chosenAc, 1, 1 );
        
        myFile.open("/homes/12007734/pvm/bankInfo", std::ios_base::app);
        myFile << "Chosen op: " << chosenOp << "\n";
        myFile << "Chosen on bank: " << bank->GetId() << "\n";
        myFile << "Chosen on account: " << chosenAc << "\n";
        myFile.close();
        
        for( int i=0; i < nbBankAccount; i++ )
        { 
            if( chosenAc == bankAccList[i]->GetId() )
            {
                chosenAc = i;
                break;
            }
        }
        
        switch( chosenOp )				// deals with operations
        {
            case ENQUIRE:
                
                pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                pvm_pkint( &chosenOp, 1, 1 );
                pvm_send( children[chosenAc], ACCOUNTOP );
                
                break;
            case DEPOSIT:
                
                pvm_upkint( &chosenAm, 1, 1 );
                
                myFile.open("/homes/12007734/pvm/bankInfo", std::ios_base::app);
                myFile << "Deposit of" << chosenAm << " on account: " << bankAccList[chosenAc]->GetId() << "\n";
                myFile.close();
                
                pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                pvm_pkint( &chosenOp, 1, 1 );
                pvm_pkint( &chosenAm, 1, 1 );
                pvm_send( children[chosenAc], ACCOUNTOP );
                
                pvm_recv( -1, ACCOUNTOPR );  /* receive data */
                pvm_upkint( &newBalance, 1, 1 );
                
                pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                pvm_pkint( &newBalance, 1, 1 );
                pvm_send( ctid, ACCOUNTOPR );
                
                
                break;
            case WITHDRAW:
                
                pvm_upkint( &chosenAm, 1, 1 );
                
                myFile.open("/homes/12007734/pvm/bankInfo", std::ios_base::app);
                myFile << "Withdrawal of" << chosenAm << " on account: " << bankAccList[chosenAc]->GetId() << "\n";
                myFile.close();
                
                pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                pvm_pkint( &chosenOp, 1, 1 );
                pvm_pkint( &chosenAm, 1, 1 );
                pvm_send( children[chosenAc], ACCOUNTOP );
                
                pvm_recv( -1, ACCOUNTOPR );  /* receive data */
                pvm_upkint( &success, 1, 1 );
                pvm_upkint( &newBalance, 1, 1 );
                
                pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                pvm_pkint( &success, 1, 1 );
                pvm_pkint( &newBalance, 1, 1 );
                pvm_send( ctid, ACCOUNTOPR );
                
                break;
            case TRANSFER:
                
                int distBank;
                int distAccount;
                int currentBalance;
                int tempOp;
                
                distAccount = -1;
                
                pvm_upkint( &distBank, 1, 1 );
                pvm_upkint( &distAccount, 1, 1 );
                pvm_upkint( &chosenAm, 1, 1 );
                
                if( distBank == bank->GetId() )			// If receiving bank account is managed by same bank
                {
                    for( int i=0; i < nbBankAccount; i++ )
                    { 
                        if( distAccount == bankAccList[i]->GetId() )
                        {
                            distAccount = i;
                            break;
                        }
                    }
                    
                    if( distAccount != -1 )			// Checks if distAccount exists
                    {
                        tempOp = WITHDRAW;

                        pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                        pvm_pkint( &tempOp, 1, 1 );
                        pvm_pkint( &chosenAm, 1, 1 );
                        pvm_send( children[chosenAc], ACCOUNTOP );

                        pvm_recv( -1, ACCOUNTOPR );  /* receive data */
                        pvm_upkint( &success, 1, 1 );  /* unpack data */
                        pvm_upkint( &currentBalance, 1, 1 );  /* unpack data */

                        if( success == 1 )				// Withdrawal successful! Continue...
                        {
                            tempOp = DEPOSIT;
                            int unwtdBalance;

                            pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                            pvm_pkint( &tempOp, 1, 1 );
                            pvm_pkint( &chosenAm, 1, 1 );
                            pvm_send( children[distAccount], ACCOUNTOP );

                            pvm_recv( -1, ACCOUNTOPR );  /* receive data */
                            pvm_upkint( &unwtdBalance, 1, 1 );
                        }
                    }
                }
                else							// Bank account in different Bank
                {
                    int banktid;
                    int tempNbBankAccount;
                    
                    pvm_spawn(
                        (char*)"/homes/12007734/pvm/nameServer",		// Need namespace
                        (char**) 0,
                        PvmTaskHost,
                        (char*)"node03",
                        1,
                        &nametid                                               
                        );
                    
                    int requestType = BKCONNECT;				// Skip asking for bank list
                    
                    pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                    pvm_pkint( &requestType, 1, 1 );
                    pvm_pkint( &distBank, 1, 1 );
                    pvm_send( nametid, NAMESVRRQST );
                    
                    pvm_recv( -1, BKLINK );
                    pvm_upkint( &banktid, 1, 1 );
                    
                    char serverPass[20];
                    
                    strcpy( serverPass, "admin" );				// To bypass login on distant bank
                    
                    // send data to server
                    pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                    pvm_pkstr( serverPass );
                    pvm_pkstr( serverPass );
                    
                    pvm_send( banktid, LOGIN );  /* send */
                    
                    /* await reply from worker */
                    info = pvm_recv( -1, LOGINR );  /* receive data */
                    info = pvm_upkint( &tempNbBankAccount, 1, 1 );  /* unpack data */ 	// number of possible bank accounts
                    
                    
                    if( tempNbBankAccount != -1 )
                    {
                        
                        int ctid[3] ,cId[3];
                        char **cName = new char*[3];
                        cName[0] = new char[10];
                        cName[1] = new char[10];
                        cName[2] = new char[10];
                        
                        for( int i=0; i < tempNbBankAccount; i++ )	Get all information need of all bank accounts
                        {    
                            pvm_upkint( &ctid[i], 1, 1 );
                            pvm_upkint( &cId[i], 1, 1 );
                            pvm_upkstr( cName[i] );
                            
                        }
                        
                        for( int i=0; i < tempNbBankAccount; i++ )
                        {    
                            if( cId[i] == distAccount )				// Id match! Proceed...
                            {
                                
                                
                                tempOp = WITHDRAW;

                                pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                                pvm_pkint( &tempOp, 1, 1 );
                                pvm_pkint( &chosenAm, 1, 1 );
                                pvm_send( children[chosenAc], ACCOUNTOP );

                                pvm_recv( -1, ACCOUNTOPR );  /* receive data */
                                pvm_upkint( &success, 1, 1 );  /* unpack data */
                                pvm_upkint( &currentBalance, 1, 1 );  /* unpack data */
                                
                                if( success == 1 )
                                {
                                    
                                    tempOp = DEPOSIT;
                                    int unwtdBalance;

                                    pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                                    pvm_pkint( &tempOp, 1, 1 );
                                    pvm_pkint( &cId[i], 1, 1 );
                                    pvm_pkint( &chosenAm, 1, 1 );
                                    pvm_send( banktid, ACCOUNTOP );

                                    pvm_recv( -1, ACCOUNTOPR );  /* receive data */
                                    pvm_upkint( &unwtdBalance, 1, 1 );
                                }
                                
                            }
                        }
                        
                    }
                    
                }
                
                pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                pvm_pkint( &success, 1, 1 );
                pvm_pkint( &currentBalance, 1, 1 );
                pvm_send( ctid, ACCOUNTOPR );
                
                break;
        }
        
    }
    
    //for(int i = 0; i < 10; i++)
    //    delete [] toSend[i];
    //delete [] toSend;
    
    cout << "Done!" ;
    
    pvm_exit();
    
    
    return 0;
}

