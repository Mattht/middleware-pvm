/* 
 * File:   BankClient.cpp
 * Author: mattht
 *
 */

#include <cstdlib>
#include <iostream>
#include <string.h>
#include <vector>
#include "pvminc.h"
#include "networkMessages.h"

using namespace std;

int main(int argc, char** argv) {
    
    int mytid, nametid, banktid;  /* task id for self and worker=tids[0] */
    int info;
    
    // Task id:
    mytid = pvm_mytid();
    
    info = pvm_spawn(
            (char*)"/homes/12007734/pvm/nameServer",
            (char**)0,
            PvmTaskHost,
            (char*)"node03",
            1,
            &nametid
            );
    
    bool bankSelected = false;
    int bkInputId;
    int chosenBkId;
    
    int nbBank;
    int bId[3];
    char **bName = new char*[3];
    bName[0] = new char[10];
    bName[1] = new char[10];
    bName[2] = new char[10];
    
    int requestType = BKLIST;
    
    pvm_initsend( PvmDataDefault );
    pvm_pkint( &requestType, 1, 1 );
    pvm_send( nametid, NAMESVRRQST );
    
    pvm_recv( -1, BKLISTR );
    pvm_upkint( &nbBank, 1, 1 );
    
    for( int i = 0; i < nbBank ; i++ )
    {
        pvm_upkint( &bId[i], 1, 1 );
        pvm_upkstr( bName[i] );
    }
    
    while( bankSelected == false )
    {
        for( int i = 0; i < nbBank ; i++ )
        {
            cout << bId[i] << " - " << bName[i] << endl;
        }
        
        cout << "Desired Bank Id: " << endl;
        cin >> bkInputId;

        for( int i=0; i < nbBank; i++ )
        { 
            if( bkInputId == bId[i] )
                chosenBkId = i;
        }

        if( chosenBkId == -1 )
            continue;
        
        int requestType = BKCONNECT;
        
        pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
        pvm_pkint( &requestType, 1, 1);
        pvm_pkint( &chosenBkId, 1, 1 );
        pvm_send( nametid, NAMESVRRQST );
        //pvm_send( nametid, BKCONNECT );  /* send */
    
        pvm_recv( -1, BKLINK );
        pvm_upkint( &banktid, 1, 1 );
        
        bankSelected = true;
        
    }
    
    
    
    char username[20];
    char password[20];
    
    int ctid[3] ,cId[3];
    
    char **cName = new char*[3];
    cName[0] = new char[10];
    cName[1] = new char[10];
    cName[2] = new char[10];
    
    int nbBankAccount = -1;
    bool connected = false;
    
    while( nbBankAccount == -1 )
    {
        cout << "----------" << endl;
        cout << "Please enter your username:" << endl;
        cin >> username;
        cout << "Please enter your password:" << endl;
        cin >> password;

        // send data to server
        pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
        pvm_pkstr( username );
        pvm_pkstr( password );
        
        cout << banktid << endl;
        
        pvm_send( banktid, LOGIN );  /* send */

        //cout << "Connecting..." << endl;
        //cout << "pvm_spawn = " << info << " worker tid = " << banktid << endl;

        /* await reply from worker */
        info = pvm_recv( -1, LOGINR );  /* receive data */
        info = pvm_upkint( &nbBankAccount, 1, 1 );  /* unpack data */
        
        cout << "----------" << endl;
        if( nbBankAccount == -1 )
        {
            cout << "Connection refused." << endl;
        }
        else
        {
            for( int i=0; i < nbBankAccount; i++ )
            {    
                pvm_upkint( &ctid[i], 1, 1 );
                pvm_upkint( &cId[i], 1, 1 );
                pvm_upkstr( cName[i] );
                
                /*cout << "Account added: " << ctid[i] << endl;
                cout << "Account id: " << cId[i] << endl;
                cout << "Account name: " << cName[i] << endl;
                cout << "-----" << endl;*/
            }
            cout << "Welcome " << username << endl;
            cout << "__________" << endl;
            
            connected = true;
        }
    }
    
    while( connected )
    {
        int inputId;
        int chosenId = -1;
        int chosenOp = -1;
        int chosenAmount = -1;
        int currentBalance = -1;
        int distBank = 0;
        int distAccount = -1;
        
        for( int i=0; i < nbBankAccount; i++ )
        { 
            cout << cId[i] << " - " << cName[i] << endl;
        }
        
        cout << "Desired account ID:" << endl;
        cin >> inputId;
        
        for( int i=0; i < nbBankAccount; i++ )
        { 
            if( inputId == cId[i] )
                chosenId = i;
        }
        
        if( chosenId == -1 )             // Account ID was wrongly typed, restart!
            continue;
        
        while( chosenOp < 0 || chosenOp >= 5 )
        {
            cout << "----------" << endl << "Account Chosen: " << cId[chosenId] << " - " << cName[chosenId] << endl;
            cout << "Available Operations:" << endl << "0 - Enquire" << endl << "1 - Deposit" << endl << "2 - Withdraw" << endl << "3 - Transfer" << endl;
            cout << "Desired Operation Id:" << endl;
            cin >> chosenOp;
            cin.ignore();
        }
        
        switch( chosenOp )
        {
            case ENQUIRE:
                
                cout << "Enquiring..." << endl;
                
                pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                pvm_pkint( &chosenOp, 1, 1 );
                pvm_pkint( &cId[chosenId], 1, 1 );
                pvm_send( banktid, ACCOUNTOP );
                
                pvm_recv( -1, ACCOUNTOPR );  /* receive data */
                pvm_upkint( &currentBalance, 1, 1 );  /* unpack data */
                
                cout << "Current Balance: " << currentBalance << endl;
                
                break;
            case DEPOSIT:
                cout << "Enter Amount:" << endl;
                cin >> chosenAmount;
                cin.ignore();
                
                cout << "Depositing..." << endl;
                
                pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                pvm_pkint( &chosenOp, 1, 1 );
                pvm_pkint( &cId[chosenId], 1, 1 );
                pvm_pkint( &chosenAmount, 1, 1 );
                pvm_send( banktid, ACCOUNTOP );
                
                pvm_recv( -1, ACCOUNTOPR );  /* receive data */
                pvm_upkint( &currentBalance, 1, 1 );  /* unpack data */
                
                cout << "£" << chosenAmount << " deposited!" << endl;
                cout << "New Balance: " << currentBalance << endl;
                
                break;
            case WITHDRAW:
                int success;
                
                cout << "Enter Amount:" << endl;
                cin >> chosenAmount;
                cin.ignore();
                
                cout << "Withdrawing..." << endl;
                
                pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                pvm_pkint( &chosenOp, 1, 1 );
                pvm_pkint( &cId[chosenId], 1, 1 );
                pvm_pkint( &chosenAmount, 1, 1 );
                pvm_send( banktid, ACCOUNTOP );
                
                pvm_recv( -1, ACCOUNTOPR );  /* receive data */
                pvm_upkint( &success, 1, 1 );  /* unpack data */
                pvm_upkint( &currentBalance, 1, 1 );  /* unpack data */
                
                
                if( success == 1 )
                {
                    cout << "£" << chosenAmount << " withdrawn!" << endl;
                    cout << "New Balance: " << currentBalance << endl;
                }
                else
                {
                    cout << "Insufficient Funds" << endl;
                    cout << "Current Balance: " << currentBalance << endl;
                }
                
                break;
            case TRANSFER:
                
                cout << "Bank ID:" << endl;                               
                cin >> distBank;
                cout << "Account ID:" << endl;                            // User will know in advance bank account
                cin >> distAccount;
                cout << "Amount:" << endl;
                cin >> chosenAmount;
                
                cin.ignore();
                
                cout << "Transferring..." << endl;
                
                pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                pvm_pkint( &chosenOp, 1, 1 );
                pvm_pkint( &cId[chosenId], 1, 1 );
                pvm_pkint( &distBank, 1, 1 );
                pvm_pkint( &distAccount, 1, 1 );
                pvm_pkint( &chosenAmount, 1, 1 );
                pvm_send( banktid, ACCOUNTOP );
                
                pvm_recv( -1, ACCOUNTOPR );  /* receive data */
                pvm_upkint( &success, 1, 1 );  /* unpack data */
                pvm_upkint( &currentBalance, 1, 1 );  /* unpack data */
                
                
                if( success == 1 )
                {
                    cout << "£" << chosenAmount << " transferred!" << endl;
                    cout << "New Balance: " << currentBalance << endl;
                }
                else
                {
                    cout << "Insufficient Funds" << endl;
                    cout << "Current Balance: " << currentBalance << endl;
                }
                
                break;
        }
        
        cin.ignore(); // Used to give the user time to look at result before continuing
        
    }
    
    return 0;
}

