PVMLIBS = /usr/lib/libpvm3.a
CC = g++

all: driver nameServer worker bankAccount
driver: BankClient.cpp
	$(CC) -o driver BankClient.cpp $(PVMLIBS)
nameServer: NameServerProcess.cpp Bank.cpp
	$(CC) -o nameServer NameServerProcess.cpp $(PVMLIBS) Bank.cpp
worker: BankProcess.cpp Bank.cpp BankAccount.cpp
	$(CC) -o worker BankProcess.cpp $(PVMLIBS) Bank.cpp BankAccount.cpp
bankAccount: AccountProcess.cpp BankAccount.cpp
	$(CC) -o bankAccount AccountProcess.cpp $(PVMLIBS) BankAccount.cpp
