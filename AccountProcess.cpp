/* 
 * File:   AccountProcess.cpp
 * Author: mattht
 *
 */

#include <cstdlib>
#include <iostream>
#include <string.h>
#include <fstream>
#include "pvminc.h"
#include "networkMessages.h"
#include "BankAccount.h"

using namespace std;

int main(int argc, char** argv) {
    
    BankAccount *bankAccount;
    
    bankAccount = new BankAccount( atoi(argv[2]), argv[3], atoi(argv[4]) );
    
    int mytid;
    int ctid; // Client task id
    int hqtid; // Bank HQ task id;
    int info;
    
    mytid = pvm_mytid();
    hqtid = pvm_parent();
    ctid = atoi(argv[1]);
    
    //int balance = atoi(argv[4]);
    
    ofstream accFile;
    accFile.open("/homes/12007734/pvm/accountinfo", std::ios_base::app);
    accFile << ctid << "\n";
    accFile << argv[2] << "\n";
    accFile << argv[3] << "\n";
    accFile << bankAccount->GetBalance() << "\n";
    accFile << "------\n";
    accFile.close();

    int timeout;    

    timeval tmout;
    tmout.sec = 900;
    tmout.usec = 0;

    bool connected = true;
    
    while( connected )
    {
        int currentOp = -1;
        int opAmmount = 0;
        
        timeout = pvm_trecv( -1, ACCOUNTOP, &tmout );  /* receive data */
        
	if( timeout == 0 )	// No message received after 15 minutes
	{
	    connected = false;
	}
	else
	{
	pvm_upkint( &currentOp, 1, 1 );	// Operation Id
        
        switch( currentOp ) {
            case ENQUIRE:
                
                pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                pvm_pkint( bankAccount->GetBalanceForMsg(), 1, 1 );
                pvm_send( ctid, ACCOUNTOPR );
                
                break;
            case DEPOSIT:
                
                pvm_upkint( &opAmmount, 1, 1 );
                
                bankAccount->Deposit( opAmmount );
                
                pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                pvm_pkint( bankAccount->GetBalanceForMsg(), 1, 1 );
                pvm_send( hqtid, ACCOUNTOPR );
                
                break;
            case WITHDRAW:
                
                int success = 0;
                
                pvm_upkint( &opAmmount, 1, 1 );
                
                if( bankAccount->HasSufficientFunds( opAmmount ) )
                {
                    success = 1;
                    bankAccount->Withdraw( opAmmount );
                }
                
                pvm_initsend(PvmDataDefault);  // initialise pvm buffer to send
                pvm_pkint( &success, 1, 1 );
                pvm_pkint( bankAccount->GetBalanceForMsg(), 1, 1 );
                pvm_send( hqtid, ACCOUNTOPR );
                
                break;
        }
        }
    }
    
    pvm_exit();
    
    
    return 0;
}

