/* 
 * File:   BankAccount.h
 * Author: mattht
 *
 */

#ifndef BANKACCOUNT_H
#define	BANKACCOUNT_H

#include <string.h>

class BankAccount {
public:
    BankAccount();
    BankAccount( int _id, const char* _name, int _balance );
    BankAccount(const BankAccount& orig);
    virtual ~BankAccount();
    
    int GetId();
    int* GetIdForMsg();
    char* GetName();
    int GetBalance();
    int* GetBalanceForMsg();
    void Deposit( int _ammount );
    bool HasSufficientFunds( int _ammount );
    void Withdraw( int _ammount );
    
private:
    int id, tid, balance;
    char name[10];
};

#endif	/* BANKACCOUNT_H */

