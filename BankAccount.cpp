/* 
 * File:   BankAccount.cpp
 * Author: mattht
 * 
 */

#include "BankAccount.h"

BankAccount::BankAccount() 
{    
    this->id = 0;
    //this->name = (char) 0;
    this->tid = 0;
    
}

BankAccount::BankAccount( int _id, const char* _name, int _balance ) 
{    
    this->id = _id;
    strcpy( this->name, _name );
    this->balance = _balance;
}

BankAccount::BankAccount(const BankAccount& orig) {
}

int BankAccount::GetId()
{
    return this->id;
}

int* BankAccount::GetIdForMsg()
{
    return &this->id;
}

char* BankAccount::GetName()
{
    return this->name;
}

int BankAccount::GetBalance()
{
    return this->balance;
}

int* BankAccount::GetBalanceForMsg()
{
    return &this->balance;
}

void BankAccount::Deposit( int _ammount )
{
    this->balance += _ammount;
}

bool BankAccount::HasSufficientFunds( int _ammount )
{
    return ( ( this->balance - _ammount ) >= 0 );
}
    
void BankAccount::Withdraw( int _ammount )
{
    this->balance -= _ammount;
}

BankAccount::~BankAccount(){
    
}
