/* 
 * File:   Bank.h
 * Author: mattht
 *
 */

#ifndef BANK_H
#define	BANK_H

#include <string.h>
#include <cstdio>

class Bank {
public:
    Bank();
    Bank( int _id, const char* _name );
    virtual ~Bank();
    
    int GetId();
    int* GetIdForMsg();
    char* GetName();
    void SetNetId( int _netId );
    int getNetId();
    void SetAddress( const char* _address );
    char* GetAddress();
    void SetFullAddress( const char* _fullAddress, int _port );
    void SetFullAddress( const char* _fullAddress );
    char* GetFullAddress();
    
private:
    int id, netId;
    char name[10], address[30], fullAddress[30];
};

#endif	/* BANK_H */

