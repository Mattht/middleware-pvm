/* 
 * File:   NameServerProcess.cpp
 * Author: mattht
 *
 */

#include <cstdlib>
#include <iostream>
#include <string.h>
#include <vector>
#include <stdio.h>
#include "pvminc.h"
#include "networkMessages.h"
#include "Bank.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int mytid, banktid;
    int ctid; // Client task id
    int info;
    
    mytid = pvm_mytid();
    ctid = pvm_parent();
    
    int nbBank;
    int wantedBank;
    
    nbBank = 3;
    
    Bank *bankList[3];
    
    bankList[0] = new Bank( 101, "Barclays" );
    bankList[1] = new Bank( 102, "Loyds" );
    bankList[2] = new Bank( 103, "Coop" );
    
    bool requestComplete = false;
    
    while( requestComplete == false)
    {
        int requestType;

        pvm_recv( -1, NAMESVRRQST );
        pvm_upkint( &requestType, 1, 1 );

        switch( requestType )
        {
            case BKLIST:
                pvm_initsend( PvmDataDefault );
                pvm_pkint( &nbBank, 1, 1 );

                for( int i = 0; i < nbBank; i++ ) {
                    pvm_pkint( bankList[i]->GetIdForMsg(), 1, 1 ); // ACCOUNT ID
                    pvm_pkstr( bankList[i]->GetName() ); // ACCOUNT NAME
                }

                pvm_send( ctid, BKLISTR );
                break;
            case BKCONNECT:
                pvm_upkint( &wantedBank, 1, 1 );
                
                if( wantedBank >= nbBank )
                {
                    for( int i = 0 ; i < nbBank ; i++ )
                    {
                        if( wantedBank == bankList[i]->GetId() )
                        {
                            wantedBank = i;
                        }
                    }
                }
                
                char **toSend;

                toSend = new char*[4];
                for(int j = 0; j < 4; j++)
                    toSend[j] = new char[10];

                sprintf( toSend[0], "%d", ctid );                           // Client tid
                sprintf( toSend[1], "%d", bankList[wantedBank]->GetId() );                // Bank Id
                strcpy( toSend[2], bankList[wantedBank]->GetName() );                      // Bank Name + SEND ACCOUNTS
                toSend[3] = (char*)0;

                char nodeInfo[10];
                
                if( bankList[wantedBank]->GetId() == 101 )	// Sets a node depending on Bank Id
                {
                    strcpy( nodeInfo, "node04" );
                }
                else if ( bankList[wantedBank]->GetId() == 102 )
                {
                    strcpy( nodeInfo, "node05" );
                }
                else if ( bankList[wantedBank]->GetId() == 103 )
                {
                    strcpy( nodeInfo, "node06" );
                }
                else
                    strcpy( nodeInfo, "node04" );

                info = pvm_spawn(
                        (char*)"/homes/12007734/pvm/worker",
                        toSend,
                        PvmTaskHost,
                        nodeInfo,
                        1,
                        &banktid
                        );

                pvm_initsend( PvmDataDefault );
                pvm_pkint( &banktid, 1, 1 );

                pvm_send( ctid, BKLINK );

                requestComplete = true;
                break;
        }
    }
    
    pvm_exit();
    
    return 0;
}

