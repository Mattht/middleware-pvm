/* 
 * File:   networkMessages.h
 * Author: mattht
 *
 * Created on 25 October 2012, 12:34
 */

#ifndef NETWORKMESSAGES_H
#define	NETWORKMESSAGES_H

enum NetworkMessage { NAMESVRRQST, BKLISTR, BKLINK, LOGIN, LOGINR, INITTRANSFER, ACCOUNTOP, ACCOUNTOPR };

enum RequestType { BKLIST, BKCONNECT };

enum OperationId { ENQUIRE, DEPOSIT, WITHDRAW, TRANSFER  };

#endif	/* NETWORKMESSAGES_H */

